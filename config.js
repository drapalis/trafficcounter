module.exports = {
    host: '212.24.106.69',
    port: '28015',
    database: 'traffic_counter',

    serverPort: 3211,

    emailValidation: /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i,
    hashChars: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_',
    salt: (length, hashChars) => {
        if(!hashChars) throw "Salting error, hashChars not provided";
        if(!length) length = 16;
        let salt = '';
        for(let i = 0; i < length; i++) {
            salt = salt + hashChars.charAt(Math.round(Math.random()*(hashChars.length-1)));
        }
        return salt;
    },

    emailSalt: '$2a$10$Yr/7TRUDhR0htEj/pxu7R.',

    requestLimit: 10000,
    requestLimitExceptions: [
        '212.24.106.69'
    ],

    logger: {
        log: {
            logToConsole: true,
            logToFile: false
        },
        error: {
            logToConsole: true,
            logToFile: false
        },
        request: {
            logToConsole: true,
            logToFile: false
        }
    }
};