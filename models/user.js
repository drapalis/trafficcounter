module.exports = {

    getUserData: (object) => {

        let user = {};

        user.id = object.id || null;
        user.userID = object.userID || null;
        user.email = object.email || null;
        user.password = object.password || null;
        user.salt = object.salt || null;
        user.google = object.google || null;

        return user;
    },

    getUserListData: (object) => {

        let users = [];

        for(let i = 0; i < object.length; i++) users.push(module.exports.getUserData(object[i]));

        return users;
    }

};