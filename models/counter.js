module.exports = {

    getCounterData: (object) => {

        let counter = {};

        counter.id = object.id || null;
        counter.userID = object.userID || null;
        counter.counterID = object.counterID || null;
        counter.title = object.title || '-';
        counter.url = object.url || null;
        counter.description = object.description || '-';
        counter.backgroundColor = object.backgroundColor || null;
        counter.foregroundColor = object.foregroundColor || null;
        counter.views = object.views || 0;
        counter.uniqueViews = object.uniqueViews || 0;
        counter.viewsToday = object.viewsToday || 0;
        counter.uniqueViewsToday = object.uniqueViewsToday || 0;

        return counter;
    },

    getCounterListData: (object) => {

        let counters = [];

        for(let i = 0; i < object.length; i++) counters.push(module.exports.getCounterData(object[i]));

        return counters;
    }

};