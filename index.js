const express = require('express');
const bodyParser = require('body-parser');
const config = require("./config");
const app = express();
const response = require('./module/response');
const logger = require('./module/log');

app.use(bodyParser.json());

let limits = {};

// REQUIRE ROUTES
const user = require('./routes/user');
const counter = require('./routes/counter');
const stat = require('./routes/stat');
const website = require('./routes/website');
const token = require('./routes/token');

//LIMITS AND EXCEPTIONS
app.use("/favicon.ico", (req, res) => { res.end(); });

app.use('/*', (req, res, next) => {
    logger.request("URL: " + req.params[0] + ", BODY: " + JSON.stringify(req.body));
    /* getting ip */
    let ip = req.connection.remoteAddress.split(":").splice(-1)[0];
    /* checking for ip exceptions */
    if(config.requestLimitExceptions.indexOf(ip) !== -1) return next();
    /* adding ip to list if does not exists */
    if(!limits[ip]) limits[ip] = 0;
    /* checking if request limit for ip was reached */
    if(limits[ip] < config.requestLimit) {
        /* increasing request counter */
        limits[ip] += 1;
        /* continuing code */
        next();
    } else {
        /* responding with too mane request response */
        response.tooManyRequests(res);
    }
});

// SET ROUTES
app.use('/user', user);
app.use('/counter', counter);
app.use('/stat', stat);
app.use('/website', website);
app.use('/token', token);


// UNDEFINED ROUTES FALLBACK
app.use('/*', (req, res) => {
    response.invalidRequest(res);
});

app.listen(config.serverPort, () => {
    console.log("Running on port %s", config.serverPort);
});

setInterval(() => {
    limits = {};
}, 1000*60);