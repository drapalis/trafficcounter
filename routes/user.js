/*
*
* User router
*
* Routes:
*   GET:
*       user/[userToken]
*       user/[masterToken]
*       user/[userID]/[masterToken]
*           or user/[userID]/[userToken] if token holder and userID matches
*
*   POST:
*       user/[masterToken]
*
*   PUT:
*       user/[userID]/[masterToken]
*           or user/[userID]/[userToken] if token holder and userID matches
*       user/[userToken]
*
*   DELETE:
*       user/[userID]/[masterToken]
*           or user/[userID]/[userToken] if token holder and userID matches
*       user/[userToken]
*
* */

'use strict';
const express = require('express');
const router = express.Router();
const r = require('rethinkdb');
const config = require('../config');
const user = require('../models/user');
let tokenModule = require('../module/token');
let userModule = require('../module/user');
const response = require('../module/response');
const logger = require('../module/log');

/*
* Connecting to the database
* */

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if (err) throw err;
    connection = conn;
});

/*
* No API token provided
* */

router.get('/', (req, res) => {
    response.BAD_REQUEST(res, {ERROR: "Please provide an API key"});
});

/*
* Getting user data if token with USER access level is given
* or getting a list of all users ans their data if token with MASTER access level is given
* */

router.get('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then((data) => {
            /* Token exists, now checking the access level */
            switch (data.accessLevel) {
                case "USER":
                    /* Given token has USER access level. Getting token holder's data */
                    userModule.getUserByID(data.holder)
                        .then(result => response.OK(res, user.getUserData(result)))
                        .catch(err => response.manageResponse(res, err));
                    break;
                case "MASTER":
                    /* Given token has MASTER access level. Getting a list of users and their data */
                    userModule.getUserList()
                        .then(result => response.OK(res, user.getUserListData(result)))
                        .catch(err => response.manageResponse(res, err));
                    break;
            }
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Getting single user data if token with MASTER access level is given
* or if token has USER access level and given user ID matches token holder
* */
router.get('/:id/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then((token) => {
            /* Token exists, now checking the access level or if token belongs to given user */
            if (token.accessLevel === "MASTER" || token.holder === req.params.id) {
                /*  Getting user's data */
                userModule.getUserByID(req.params.id)
                    .then(result => response.OK(res, user.getUserData(result)))
                    .catch(err => response.manageResponse(res, err));
            }
            else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Creating a new user if token with MASTER access level is provided
* Required data for successful creation: email, password with salt or google id
* */
router.post('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then((token) => {
            /* Token exists, now checking the access level */
            if (token.accessLevel === 'MASTER') {
                /* Creating new user */
                userModule.signUp(user.getUserData(req.body))
                    .then(result => response.CREATED(res, result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Updating user data if token with MASTER access level is given
* or if token has USER access level and given user ID matches token holder
* */
router.put('/:id/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then(token => {
            /* Token exists, now checking the access level or if token belongs to given user */
            if(token.accessLevel === 'MASTER' || token.holder === req.params.id) {
                /* Updating user data */
                userModule.updateUser(req.params.id, req.body)
                    .then(result => response.OK(res,result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Updating user data if token has USER access level
* */
router.put('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then(token => {
            /* Token exists, now checking if token has USER access level */
            if(token.accessLevel === 'USER') {
                /* Updating user data */
                userModule.updateUser(token.holder, req.body)
                    .then(result => response.OK(res,result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Deleting user data if token with MASTER access level is given
* or if token has USER access level and given user ID matches token holder
* */
router.delete('/:id/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then(token => {
            /* Token exists, now checking the access level or if token belongs to given user */
            if(token.accessLevel === 'MASTER' || token.holder === req.params.id) {
                /* Deleting user data */
                userModule.deleteUser(req.params.id)
                    .then(result => response.OK(res,result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Deleting user data if token has USER access level
* */
router.delete('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then(token => {
            /* Token exists, now checking if token has USER access level */
            if(token.accessLevel === 'USER') {
                /* Deleting user data */
                userModule.deleteUser(token.holder)
                    .then(result => response.OK(res,result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

module.exports = router;