const express = require('express');
const router = express.Router();
const r = require('rethinkdb');
const config = require('../config');
const response = require('../module/response');
const logger = require('../module/log');
let tokenModule = require('../module/token');
const website = require('../module/website');

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if(err) throw err;
    connection = conn;
});

/*
* getting website's stats
* */

router.get('/', (req, res) => {
    website.getStats().then(result => {

        /* removing unnecessary id field */
        delete result.id;

        response.OK(res, result);
    }).catch(err => response.manageResponse(res, err));
});

/*
* adding data to website's stats
* */

router.put('/:token', (req, res) => {
    /* getting token */
    tokenModule.getToken(req.params.token)
        .then(result => {
            if(result.accessLevel === 'MASTER') {

                /* token has MASTER access level */

                let data = {
                    "totalUniqueVisits": req.body.totalUniqueVisits || 0,
                    "totalVisits": req.body.totalVisits || 0,
                    "uniqueVisitsToday": req.body.uniqueVisitsToday || 0,
                    "visitsToday": req.body.visitsToday || 0
                };

                website.updateStats(data).then(result => response.OK(res, result)).catch(err => response.manageResponse(res, err));

            } else {

                /* token does not have MASTER access level */

                logger.log("routes/website.js -> put: token (" + result.id + ") does not have MASTER access level");
                response.accessDenied(res);
            }
        })
        .catch(err => response.manageResponse(err));
});

module.exports = router;