const express = require('express');
const router = express.Router();
const r = require('rethinkdb');
const config = require('../config');
const response = require('../module/response');

const stats = require('../module/stats');

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if(err) throw err;
    connection = conn;
});

//get user data
router.get('/:counterID', (req, res) => {
    stats.get(req.params.counterID, req)
        .then(result => response.OK(res, result))
        .catch(err => response.manageResponse(res, err));
});
router.post('/:id', (req, res) => {
    stats.add(req.params.id, req.body.ip, req.body.ref, req.body.tag)
        .then(result => response.OK(res, result))
        .catch(err => response.manageResponse(res, err));
});

module.exports = router;