const express = require('express');
const router = express.Router();
const r = require('rethinkdb');
const config = require('../config');
const response = require('../module/response');
const logger = require('../module/log');
const counterModel = require('../models/counter');
const counterModule = require('../module/counter');
const tokenModule = require('../module/token');

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if(err) throw err;
    connection = conn;
});

/*
* Getting counter list
* */
router.get('/', (req, res) => {
    counterModule.getCounters()
        .then(result => response.OK(res, counterModel.getCounterListData(result)))
        .catch(err => response.manageResponse(res, err));
});

/*
* Getting single counter if counter id is provided or list of counters of user if user token is provided
* */
router.get('/:id', (req, res) => {
    if(req.params.id.length === 16) {
        counterModule.getCounter(req.params.id)
            .then(result => response.OK(res, counterModel.getCounterData(result)))
            .catch(err => response.manageResponse(res, err));
    } else {
        /* Getting given token */
        tokenModule.getToken(req.params.id)
            .then((data) => {
                /* Token exists, now checking the access level */
                if (data.accessLevel === "USER") {
                    /* Given token has USER access level. Getting token holder's data */
                    counterModule.getCountersOfUser(data.holder)
                        .then(result => response.OK(res, counterModel.getCounterListData(result)))
                        .catch(err => response.manageResponse(res, err));
                }
            })
            .catch(err => response.manageResponse(res, err));
    }
});

/*
* Creating a new counter if token with USER access level is provided
* Required data for successful creation: userID and URL
* */
router.post('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then((data) => {
            /* Token exists, now checking the access level */
            if (data.accessLevel === "USER") {
                /* Given token has USER access level. Getting token holder's data */
                counterModule.createCounter(data.holder, counterModel.getCounterData(req.body))
                    .then(result => response.CREATED(res, result))
                    .catch(err => response.manageResponse(res, err));
            }
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Updating counter data if token has COUNTER access level
* */
router.put('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then((data) => {
            /* Token exists, now checking the access level */
            if (data.accessLevel === "COUNTER") {
                /* Given token has COUNTER access level. Getting token holder's data */
                counterModule.updateCounter(data.holder, counterModel.getCounterData(req.body))
                    .then(result => response.OK(res, result))
                    .catch(err => response.manageResponse(res, err));
            } else {
                response.accessDenied(res);
            }
        })
        .catch(err => response.manageResponse(res, err));
});


/*
* Deleting counter data if token with MASTER access level is given
* or if token has COUNTER access level and given counter ID matches token holder
* */
router.delete('/:id/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then(token => {
            /* Token exists, now checking the access level or if token belongs to given counter */
            if(token.accessLevel === 'MASTER' || (token.accessLevel === "COUNTER" && token.holder === req.params.id)) {
                /* Deleting counter data */
                counterModule.deleteCounter(req.params.id)
                    .then(result => response.OK(res,result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

/*
* Deleting counter data if token has COUNTER access level
* */
router.delete('/:token', (req, res) => {
    /* Getting given token */
    tokenModule.getToken(req.params.token)
        .then(token => {
            /* Token exists, now checking if token has COUNTER access level */
            if(token.accessLevel === 'COUNTER') {
                /* Deleting counter data */
                counterModule.deleteCounter(token.holder)
                    .then(result => response.OK(res,result))
                    .catch(err => response.manageResponse(res, err));
            } else response.manageResponse(res, "ACCESS_DENIED");
        })
        .catch(err => response.manageResponse(res, err));
});

module.exports = router;