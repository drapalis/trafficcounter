const express = require('express');
const router = express.Router();
const r = require('rethinkdb');
const config = require('../config');
let tokenModule = require('../module/token');
const response = require('../module/response');

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if(err) throw err;
    connection = conn;
});
/*
* Get token
* */
router.get('/:token/:holder', (req, res) => {
    tokenModule.getToken(req.params.token).then((data) => {
        switch (data.accessLevel) {
            case "MASTER":
                tokenModule.getTokenByHolder(req.params.holder).then(result => {
                    response.OK(res, result);
                }).catch(err => {
                    response.manageResponse(res, err);
                });
                break;
            default:
                response.FORBIDDEN(res, "Access denied");
        }
    }).catch(err => {
        response.manageResponse(res, err);
    });
});
/*
* Get new token
* */
router.put('/:token/:holder', (req, res) => {
    tokenModule.getToken(req.params.token).then((data) => {
        switch (data.accessLevel) {
            case "MASTER":
                tokenModule.getTokenByHolder(req.params.holder).then(result => {
                    tokenModule.deleteToken(result.id).then(() => {
                        tokenModule.newToken(result.holder, result.accessLevel)
                            .then(newToken => response.OK(res, {newToken: newToken}))
                            .catch(err => response.manageResponse(err));
                    }).catch(err => response.manageResponse(err));
                    console.log(result);
                }).catch(err => {
                    response.manageResponse(res, err);
                });
                break;
            default:
                response.FORBIDDEN(res, "Access denied");
        }
    }).catch(err => {
        response.manageResponse(res, err);
    });
});

module.exports = router;