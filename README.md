# Traffic Counter

##User


Get user data
```
GET: http://127.0.0.1:3211/user/[USER_TOKEN]
```
Response
```
{
    "RESPONSE": {
        "id": "a9bcd931-b167-44f2-a40a-0b4574de6b9d",
        "userID": "7jz9lLKOlywoLQRW",
        "email": "drapalis@sgmail.com",
        "password": "password",
        "salt": "salt",
        "google": "13216541321654132"
    }
}
```
#
Get user list
```
GET: http://127.0.0.1:3211/user/[MASTER_TOKEN]
```
Response
```
{
    "RESPONSE": [
        {
            "id": "48037021-638d-4f0f-9c0b-9693b5791aa1",
            "userID": "cFYykxpn6aP5kbOO",
            "email": "drapalis@gmail.com",
            "password": null,
            "salt": "$2a$10$bQfqpBxm/qvl.df0DMDOru",
            "google": "115437766322612704711"
        },
        {
            "id": "a9bcd931-b167-44f2-a40a-0b4574de6b9d",
            "userID": "7jz9lLKOlywoLQRW",
            "email": "drapalis@sgmail.com",
            "password": "password",
            "salt": "salt",
            "google": "13216541321654132"
        }
    ]
}
```
#
Get user data
```
GET: http://127.0.0.1:3211/user/[USER_ID]/[MASTER_TOKEN]
```
Response
```
{
    "RESPONSE": {
        "id": "48037021-638d-4f0f-9c0b-9693b5791aa1",
        "userID": "cFYykxpn6aP5kbOO",
        "email": "drapalis@gmail.com",
        "password": null,
        "salt": "$2a$10$bQfqpBxm/qvl.df0DMDOru",
        "google": "115437766322612704711"
    }
}
```
#
Create user
```
POST: http://127.0.0.1:3211/user/[MASTER_TOKEN]
```
Request data
```
{
	"email": "drapalis@sgmail.com",
	"password": "password",
	"salt": "salt",
	"google": "13216541321654132"
}
```
Response
```
{
    "RESPONSE": {
        "id": "7jz9lLKOlywoLQRW",
        "token": "870e904f-2927-468d-b4fe-7bb819926cef"
    }
}
```
#
Update user
```
PUT: http://127.0.0.1:3211/user/[USER_ID]/[MASTER_TOKEN]
```
OR
```
PUT: http://127.0.0.1:3211/user/[USER_TOKEN]
```
Request data
```
{
	"password": "newPaasword",
	"salt": "mewSalt"
}
```
Response
```
{
    "RESPONSE": {
        "RESULT": "No data was changed"
    }
}
```
#
Delete user
```
DELETE: http://127.0.0.1:3211/user/[USER_ID]/[MASTER_TOKEN]
```
OR
```
DELETE: http://127.0.0.1:3211/user/[USER_TOKEN]
```
Response
```
{
    "RESPONSE": "USER_DELETED"
}
```
## Counter
Get counter list
```
GET: http://127.0.0.1:3211/counter
```
OR to get specific user's counters
```
GET: http://127.0.0.1:3211/counter/[userToken]
```
Response
```
{
    "RESPONSE": [
        {
            "id": "333eae3d-cf6c-4f20-a4b6-a0b2653b1e85",
            "userID": "cFYykxpn6aP5kbOO",
            "counterID": "eNMO5aHehRVN_4Gw",
            "title": "Miegalius",
            "url": "http://miegalius.eu",
            "description": "Rąstinių namelių nuoma Šventojoje",
            "backgroundColor": "#ffffff",
            "foregroundColor": "#000000",
            "views": 0,
            "uniqueViews": 0,
            "viewsToday": 0,
            "uniqueViewsToday": 0
        },
        {
            "id": "b698abc1-e5a9-405b-b2ed-5887efbda9f8",
            "userID": "cFYykxpn6aP5kbOO",
            "counterID": "EuXVJqg.lXASIb17",
            "title": "RDarius.lt",
            "url": "http://rdarius.lt",
            "description": "-",
            "backgroundColor": "#ffffff",
            "foregroundColor": "#000000",
            "views": 30,
            "uniqueViews": 1,
            "viewsToday": 30,
            "uniqueViewsToday": 1
        }
    ]
}
```
#
Get counter
```
GET: http://127.0.0.1:3211/counter/[COUNTER_ID]
```
Response
```
{
    "RESPONSE": {
        "id": "333eae3d-cf6c-4f20-a4b6-a0b2653b1e85",
        "userID": "cFYykxpn6aP5kbOO",
        "counterID": "eNMO5aHehRVN_4Gw",
        "title": "Miegalius",
        "url": "http://miegalius.eu",
        "description": "Rąstinių namelių nuoma Šventojoje",
        "backgroundColor": "#ffffff",
        "foregroundColor": "#000000",
        "views": 0,
        "uniqueViews": 0,
        "viewsToday": 0,
        "uniqueViewsToday": 0
    }
}
```
#
Create counter
```
POST: http://127.0.0.1:3211/counter/[USER_TOKEN]
```
Request data
```
{
	"url": "http://rdarius.lt",
	"title": "RDarius.lt",
	"description": "Portfolio",
	"foregroundColor": "#FFFFFF",
	"backgroundColor": "#000000"
}
```
Response
```
{
    "RESPONSE": {
        "counter": {
            "id": "UOMRfj38TXfTz1YP",
            "token": "8ab3daed-35e0-472d-86f0-925aef8072fe"
        }
    }
}
```
#
Update counter
```
PUT: http://127.0.0.1:3211/counter/[COUNTER_TOKEN]
```
Request data
```
{
	"url": "http://rdarius.lt",
	"title": "RDarius"
}
```
Response
```
{
    "RESPONSE": {
        "UPDATED": {
            "title": "RDarius",
            "url": "http://rdarius.lt"
        }
    }
}
```
#
Delete counter
```
DELETE: http://127.0.0.1:3211/counter/[COUNTER_TOKEN]
```
Response
```
{
    "RESPONSE": "COUNTER_DELETED"
}
```
#Counter Stats
Get counter stats
```
GET: http://127.0.0.1:3211/stat/[COUNTER_ID](?from=[dateTime]&to=[dateTime])
```
Response
```
{
    "RESPONSE": [
        {
            "counterID": "EuXVJqg.lXASIb17",
            "day": 12,
            "hour": 17,
            "id": "153f3ee1-0f33-4df4-8d8c-4f558691826f",
            "ip": "0.0.0.0",
            "minute": 0,
            "month": 11,
            "ref": "http://cntr.ppj.lt/",
            "second": 46,
            "tag": "none",
            "year": 2017
        },
        ...
        {
            "counterID": "EuXVJqg.lXASIb17",
            "day": 12,
            "hour": 17,
            "id": "e195c38e-6e8c-40d1-ae25-d15578e46ad4",
            "ip": "0.0.0.0",
            "minute": 1,
            "month": 11,
            "ref": "unknown",
            "second": 39,
            "tag": "none",
            "year": 2017
        }
    ]
}
```
#
Add counter stat
```
POST: http://127.0.0.1:3211/stat/[COUNTER_ID]
```
Request data
```
{
	"ip": "192.168.0.104",
	"ref": "/",
	"tag": "homepage"
}
```
Response
```
{
    "RESPONSE": {
        "INSERTED": {
            "counterID": "smEavmAibDNCKxjc",
            "ip": "192.168.0.104",
            "ref": "/",
            "tag": "homepage",
            "year": 2017,
            "month": 11,
            "day": 12,
            "hour": 11,
            "minute": 25,
            "second": 11
        }
    }
}
```
##Site Stats
Get site stats
```
GET: http://127.0.0.1:3211/website
```
Response
```
{
    "RESPONSE": {
        "totalUniqueVisits": 1,
        "totalVisits": 2,
        "uniqueVisitsToday": 3,
        "visitsToday": 4
    }
}
```
#
Update site stat
```
PUT: http://127.0.0.1:3211/website/[MASTER_TOKEN]
```
Request data
```
{
    "totalVisits": 1,
    "totalUniqueVisits": 2,
    "visitsToday": 3,
    "uniqueVisitsToday": 4
}
```
Response
```
{
    "RESPONSE": {
        "totalUniqueVisits": 1,
        "totalVisits": 2,
        "uniqueVisitsToday": 3,
        "visitsToday": 5
    }
}
```
##Token
Get user token
```
GET: http://127.0.0.1:3211/token/[MASTER_TOKEN]/[USER_ID]
```
Response
```
{
    "RESPONSE": {
        "accessLevel": "USER",
        "holder": "cFYykxpn6aP5kbOO",
        "id": "057033c4-fa0d-4214-89c3-bbf31f52c63e"
    }
}
```
#
Get counter token
```
GET: http://127.0.0.1:3211/token/[MASTER_TOKEN]/[COUNTER_ID]
```
Response
```
{
    "RESPONSE": {
        "accessLevel": "COUNTER",
        "holder": "smEavmAibDNCKxjc",
        "id": "a619b847-d094-4d0e-b9b5-a46d6e0e7840"
    }
}
```
#
Change user token
```
PUT: http://127.0.0.1:3211/token/[MASTER_TOKEN]/[USER_ID]
```
Response
```
{
    "RESPONSE": {
        "newToken": "7ed88343-b683-4860-8b3c-9b2de9b9308c"
    }
}
```
#
Change counter token
```
PUT: http://127.0.0.1:3211/token/[MASTER_TOKEN]/[COUNTER_ID]
```
Response
```
{
    "RESPONSE": {
        "newToken": "b53a20bb-1934-4fc8-be19-f8aedec92209"
    }
}
```