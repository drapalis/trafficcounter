const r = require('rethinkdb');
const tokenModule = require('../module/token');
const config = require('../config');
const logger = require('./log');
const {URL, URLSearchParams} = require('url');
const counterModule = require('./counter');

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if (err) throw err;
    connection = conn;
});

module.exports = {
    date: () => {
        return module.exports.parseDateTime(logger.date());
    },
    parseDate: (date) => {
        let d = date.split('-');
        return {
            year: parseInt(d[0]) || 0,
            month: parseInt(d[1]) || 1,
            day: parseInt(d[2]) || 1
        }
    },
    parseTime: (time) => {
        let t = time.split(':');
        return {
            hour: parseInt(t[0]) || 0,
            minute: parseInt(t[1]) || 0,
            second: parseInt(t[2]) || 0
        }
    },
    parseDateTime: (datetime) => {
        let now = datetime.split(' ');
        return {
            date: module.exports.parseDate(now[0]),
            time: module.exports.parseTime(now[1])
        };
    },
    get: (counterID, req) => {
        return new Promise((resolve, reject) => {
            let filter = {};
            let q = new URL(req.protocol + '://' + req.get('host') + req.originalUrl);
            let s = q.searchParams;

            let regexDate = /^((\d\d\d\d)-(\d?\d)-(\d?\d))$/g;
            let regexDateTime = /^((\d\d\d\d)-(\d?\d)-(\d?\d) (\d?\d):(\d?\d):(\d?\d))$/g;

            let from = {
                year: 0,
                month: 0,
                day: 0,
                hour: 0,
                minute: 0,
                second: 0
            };
            let to = {
                year: 0,
                month: 0,
                day: 0,
                hour: 0,
                minute: 0,
                second: 0
            };
            let search = {};
            search.from = s.get('from') || null;
            search.to = s.get('to') || null;
            if (s.get('from')) {
                if (regexDateTime.test(s.get('from'))) {
                    let date = module.exports.parseDateTime(s.get('from'));
                    from.year = parseInt(date.date.year);
                    from.month = parseInt(date.date.month);
                    from.day = parseInt(date.date.day);
                    from.hour = parseInt(date.time.hour);
                    from.minute = parseInt(date.time.minute);
                    from.second = parseInt(date.time.second);
                } else if (regexDate.test(s.get('from'))) {
                    let date = module.exports.parseDate(s.get('from'));
                    from.year = parseInt(date.date.year);
                    from.month = parseInt(date.date.month);
                    from.day = parseInt(date.date.day);
                    from.hour = 0;
                    from.minute = 0;
                    from.second = 0;
                } else {
                    reject("INVALID_REQUEST");
                    return;
                }
            }
            if (s.get('to')) {
                if (regexDateTime.test(s.get('to'))) {
                    let date = module.exports.parseDateTime(s.get('to'));
                    to.year = parseInt(date.date.year);
                    to.month = parseInt(date.date.month);
                    to.day = parseInt(date.date.day);
                    to.hour = parseInt(date.time.hour);
                    to.minute = parseInt(date.time.minute);
                    to.second = parseInt(date.time.second);
                } else if (regexDate.test(s.get('to'))) {
                    let date = module.exports.parseDate(s.get('to'));
                    to.year = parseInt(date.date.year);
                    to.month = parseInt(date.date.month);
                    to.day = parseInt(date.date.day);
                    to.hour = 23;
                    to.minute = 59;
                    to.second = 59;
                } else {
                    reject("INVALID_REQUEST");
                    return;
                }
            }
            r.table('stats').filter({counterID: counterID}).coerceTo('array').run(connection).then(result => {
                let ret = [];
                for (let x = 0; x < result.length; x++) {
                    let y, m, d, H, i, s;
                    y = parseInt(result[x].year);
                    m = parseInt(result[x].month);
                    d = parseInt(result[x].day);
                    H = parseInt(result[x].hour);
                    i = parseInt(result[x].minute);
                    s = parseInt(result[x].second);
                    if (search.from) {
                        if (y < from.year) continue;
                        if (y === from.year) {
                            if (m < from.month) continue;
                            if (m === from.month) {
                                if (d < from.day) continue;
                                if (d === from.day) {
                                    if (H < from.hour) continue;
                                    if (H === from.hour) {
                                        if (i < from.minute) continue;
                                        if (i === from.minute) {
                                            if (s < from.second) continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (search.to) {
                        if (y > to.year) return false;
                        if (y === to.year) {
                            if (m > to.month) return false;
                            if (m === to.month) {
                                if (d > to.day) return false;
                                if (d === to.day) {
                                    if (H > to.hour) return false;
                                    if (H === to.hour) {
                                        if (i > to.minute) return false;
                                        if (i === to.minute) {
                                            if (s > to.second) return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ret.push(result[x]);
                }
                resolve(ret);
            }).catch(err => reject(err));
        });
    },
    isUnique: (ip, counter) => {
        return new Promise((resolve, reject) => {
            if (ip && counter) {
                let date = module.exports.date();
                r.table('stats').filter({
                    counterID: counter,
                    ip: ip,
                    year: date.date.year,
                    month: date.date.month,
                    day: date.date.day
                })
                    .coerceTo('array').run(connection).then(result => {
                    if (result.length === 0) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }).catch(err => reject(err));

            } else {
                reject("INVALID_DATA");
            }
        });
    },
    add: (id, ip, referrer, tag) => {
        return new Promise((resolve, reject) => {
            if (!id || id.length !== 16) {
                reject("INVALID_DATA");
                return;
            }

            let data = {};
            data.counterID = id;
            data.ip = ip || '0.0.0.0';
            data.ref = referrer || 'unknown';
            data.tag = tag || 'none';

            counterModule.getCounter(id).then(counter => {
                module.exports.isUnique(data.ip, data.counterID).then(unique => {

                    if (!unique) counterModule.addView(id);
                    else counterModule.addUniqueView(id);

                    let date = module.exports.date();

                    data.year = date.date.year;
                    data.month = date.date.month;
                    data.day = date.date.day;
                    data.hour = date.time.hour;
                    data.minute = date.time.minute;
                    data.second = date.time.second;

                    r.table('stats').insert(data).run(connection).then(result => {
                        if (result.errors !== 0)
                            reject(result.first_error);
                        else
                            resolve({INSERTED: data});
                    }).catch(err => reject(err));
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }
};