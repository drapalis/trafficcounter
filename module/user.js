/*
*
* User module
*
* This module can get user by userID, get list of users, create a new user, update user data and delete user and all their data
*
* */

const r = require('rethinkdb');
const u = require('../models/user');
const config = require('../config');
let bcrypt = require('bcrypt');
let tokenModule = require('../module/token');
const logger = require('../module/log');
let counter = require('../module/counter');

/*
* Connecting to the database
* */

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if(err) throw err;
    connection = conn;
});

module.exports = {

    /* Getting user data by userID */

    getUserByID: (id) => {
        return new Promise((resolve, reject) => {
            if(!id) {

                /* userID not provided */

                logger.error("module/user.js -> getUserByID: userID was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(id.length !== 16) {

                /* invalid userID length */

                logger.error("module/user.js -> getUserByID: provided userID had incorrect length");
                reject("INVALID_USER_ID");
                return;
            }
            r.table('users').filter({userID: id}).coerceTo("array").run(connection).then(data => {
                if(data.length === 0) {

                    /* user was not found */

                    logger.log("module/user.js -> getUserByID: user not found");
                    reject("USER_NOT_FOUND");
                } else {

                    /* user was found, returning user data */

                    resolve(data[0]);
                }
            }).catch(err => {
                reject(err);
            });
        });
    },

    /* Getting user list with their data */

    getUserList: () => {
        return new Promise((resolve, reject) => {
            r.table('users').coerceTo("array").run(connection).then(data => {
                if(data.length === 0) {

                    /* no users were found */

                    logger.log("module/user.js -> getUserList: no users found");
                    reject("NO_USERS_FOUND");
                } else {

                    /* returning user list with their data */

                    resolve(data);
                }
            }).catch(err => {
                reject(err);
            });
        });
    },

    /* Creating new user */

    signUp: (data) => {
        return new Promise((resolve, reject) => {
            if(!data) {

                /* no data provided */

                logger.error("module/user.js -> signUp: no data provided");
                reject("NO_DATA_PROVIDED");
            }

            if(!/^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i.test(data.email)
                || ((!data.password || !data.salt) && !data.google)) {

                /* invalid data provided */

                logger.log("module/user.js -> signUp: invalid data provided: " + JSON.stringify(data));
                reject("INVALID_DATA");
                return;
            }

            /* generating userID */
            data.userID = bcrypt.hashSync(data.email, config.emailSalt).substr(config.emailSalt.length,16).replace('/','_');

            /* removing ID from data object, RethinkDB does not like empty or duplicate ID being passed in */
            delete data.id;

            /* checking if user already exists */

            r.table('users').filter({userID: data.userID}).coerceTo("array").run(connection)
                .then(result => {
                    if(result.length === 0) {

                        /* user does not exists, so now a new one can be created */

                        r.table('users').insert(data).run(connection)
                            .then((result) => {
                                if(result.errors !== 0) {

                                    /* failed to create a new user */

                                    logger.error("module/user.js -> signUp: Failed to create a new user with an error: " + result.first_error);
                                    reject(result.first_error);
                                    return;
                                }

                                /* User successfully created */

                                logger.log("module/user.js -> signUp: User created: " + data.userID);

                                /* Creating an API token for new user */

                                tokenModule.newToken(data.userID, "USER")
                                    .then(result => resolve({id: data.userID, token: result}))
                                    .catch(err => reject(err));
                            })
                            .catch(err => reject(err));
                    } else {

                        /* user already exists */

                        logger.log("module/user.js -> signUp: User already exists: " + data.userID);
                        reject("USER_ALREADY_EXISTS");
                    }
                })
                .catch(err => reject(err));
        });
    },

    /* Updating user data */

    updateUser: (id, data) => {
        return new Promise((resolve, reject) => {

            if(!id) {

                /* userID not provided */

                logger.error("module/user.js -> updateUser: userID was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(id.length !== 16) {

                /* invalid userID length */

                logger.error("module/user.js -> updateUser: provided userID had incorrect length");
                reject("INVALID_USER_ID");
                return;
            } else if(!data) {

                /* no data provided */

                logger.error("module/user.js -> updateUser: no data provided");
                reject("NO_DATA_PROVIDED");
            }

            /* getting user data */
            r.table('users').filter({userID: id}).coerceTo("array").run(connection).then(userData => {
                if(userData.length === 0) {

                    /* user was not found */

                    logger.log("module/user.js -> updateUser: no users found");
                    reject("NO_USERS_FOUND");
                }

                /* setting new data */
                userData = userData[0];
                let newData = {};
                if(data.password) newData.password = data.password;
                if(data.salt) newData.salt = data.salt;
                if(data.googleID) newData.googleID = data.googleID;

                if(newData !== {}) {
                    r.table('users').filter({id: userData.id}).update(newData).run(connection).then(updateData => {
                        if(updateData.errors !== 0) {

                            /* failed to create a new user */

                            logger.error("module/user.js -> signUp: Failed to create a new user with an error: " + updateData.first_error);
                            reject(updateData.first_error);
                            return;
                        } else if(updateData.unchanged === 1) {

                            /* data was not changed */

                            logger.error("module/user.js -> signUp: No data was changed");
                            reject("NOT_CHANGED");
                        }

                        /* user data was changed, returning changes */

                        resolve({UPDATED: newData});
                    }).catch(err => reject(err));
                } else {

                    /* no changeable data provided */

                    logger.log("module/user.js -> updateUser: no changeable data provided");
                    reject("NOT_CHANGED");
                }
            }).catch(err => reject(err));
        });
    },

    /* deleting user */

    deleteUser: (id) => {
        return new Promise((resolve, reject) => {
            if(!id) {

                /* userID not provided */

                logger.error("module/user.js -> deleteUser: userID was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(id.length !== 16) {

                /* invalid userID length */

                logger.error("module/user.js -> deleteUser: provided userID had incorrect length");
                reject("INVALID_USER_ID");
                return;
            }
            /* getting user data */
            r.table('users').filter({userID: id}).coerceTo("array").run(connection).then(userData => {
                if(userData.length === 0) {

                    /* user was not found */

                    logger.log("module/user.js -> deleteUser: user not found");
                    reject("USER_NOT_FOUND");
                }

                userData = userData[0];

                // TODO: delete all user data from all tables

                counter.getCountersOfUser(userData.userID).then(counters => {
                    for(let i = 0; i < counters.length; i++) {
                        counter.deleteCounter(counters[i].counterID).then(r => {}).catch(err => logger.error(err));
                    }
                }).catch(err => logger.error(err));
                /* deleting user token */
                r.table('tokens').filter({holder: userData.userID}).delete().run(connection).then(result => {
                    if(result.errors !== 0) {
                        logger.error("module/user.js -> deleteUser: " + result.first_error);
                    }
                    logger.log("module/user.js -> deleteUser: user data from 'tokens' deleted");
                }).catch(err => logger.error(err));
                /* deleting user */
                r.table('users').filter({userID: userData.userID}).delete().run(connection).then(result => {
                    if(result.errors !== 0) {
                        logger.error("module/user.js -> deleteUser: " + result.first_error);
                    }
                    logger.log("module/user.js -> deleteUser: user data from 'users' deleted");
                }).catch(err => logger.error(err));
                resolve("USER_DELETED");

            }).catch(err => reject(err));
        });
    }
};