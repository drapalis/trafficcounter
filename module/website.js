/*
*
* User module
*
* This module can get user by userID, get list of users, create a new user, update user data and delete user and all their data
*
* */


const r = require('rethinkdb');
const u = require('../models/user');
const config = require('../config');
let bcrypt = require('bcrypt');
let tokenModule = require('../module/token');
const logger = require('../module/log');

/*
* Connecting to the database
* */

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if(err) throw err;
    connection = conn;
});

module.exports = {
    getStats: () => {
        return new Promise((resolve, reject) => {
            r.table('website').coerceTo('array').run(connection)
                .then(result => {
                    if (result.length > 0) {
                        /* got some results */

                        logger.log("module/website.js -> getStats: " + JSON.stringify(result[0]));
                        resolve(result[0]);
                    } else {
                        /* didn't get any results */

                        logger.error("module/website.js -> getStats: failed to fetch data");
                        reject({ERROR: "Failed to fetch data"});
                    }
                })
                .catch(err => reject(err));
        });
    },
    updateStats: (data) => {
        return new Promise((resolve, reject) => {
            module.exports.getStats().then(stats => {
                stats.totalVisits += data.totalVisits || 0;
                stats.totalUniqueVisits += data.totalUniqueVisits || 0;
                stats.visitsToday += data.visitsToday || 0;
                stats.uniqueVisitsToday += data.uniqueVisitsToday || 0;
                r.table('website').filter({id: stats.id}).update(stats).run(connection)
                    .then(result => {
                        if(result.errors && result.errors > 0) {

                            /* error while updating */

                            logger.error("module/website.js -> updateStats: " + result.first_error);
                            reject(result.first_error)
                        } else if(result.unchanged === 1) {

                            /* data was not changed */

                            logger.log("module/website.js -> updateStats: data unchanged");
                            reject("NOT_CHANGED");

                        } else {

                            /* removing unnecessary id field */

                            delete stats.id;

                            /* updated and returning new data */

                            logger.log("module/website.js -> updateStats: data changed: " + JSON.stringify(stats));
                            resolve(stats);

                        }
                    })
                    .catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }
};