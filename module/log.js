let fs = require('fs');
let config = require('../config');
module.exports = {
    date: () => {
        return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    },
    log: (data) => {
        let date = module.exports.date();

        if(config.logger.log.logToFile)
            fs.appendFile('log.log', date + '> ' + data + "\n", err => { if (err) module.exports.error(err); });

        if(config.logger.log.logToConsole)
            console.log('\x1b[1m\x1b[35m' + date + '>\x1b[33m' + ' ' + data + '\x1b[0m');
    },
    error: (data) => {
        let date = module.exports.date();

        if(config.logger.error.logToFile)
            fs.appendFile('error.log', date + '> ' + data + "\n", err => { if (err) throw err; });

        if(config.logger.error.logToConsole)
            console.log('\x1b[1m\x1b[35m' + date + '>\x1b[31m', data, '\x1b[0m');
    },
    request: (data) => {
        let date = module.exports.date();

        if(config.logger.request.logToFile)
            fs.appendFile('error.log', date + '> ' + data + "\n", err => { if (err) throw err; });

        if(config.logger.request.logToConsole)
            console.log('\x1b[1m\x1b[35m' + date + '>\x1b[36m', data, '\x1b[0m');
    }
};