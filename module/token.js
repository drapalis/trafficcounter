/*
* Token module
*
* This module can get a token data by token or userID, create new token.
*
* */

'use strict';
const r = require('rethinkdb');
const config = require('../config');
const logger = require('../module/log');

/*
* Connecting to the database
* */

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if (err) throw err;
    connection = conn;
});

module.exports = {

    /* Getting token data when token is given */

    getToken: (token) => {
        return new Promise((resolve, reject) => {
            if(!token) {

                /* token not provided */

                logger.error("module/token.js -> getToken: token was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(token.length !== 36) {

                /* invalid token length */

                logger.error("module/token.js -> getToken: provided token had incorrect length");
                reject("INVALID_API_TOKEN");
                return;
            }
            r.table('tokens').filter({id: token}).coerceTo("array").run(connection).then(data => {
                if (data.length === 0) {

                    /* token not found */

                    logger.log("module/token.js -> getToken: Token not found - " + token);
                    reject("INVALID_API_TOKEN");
                } else {

                    /* token is valid and exists, returning token data */

                    logger.log("module/token.js -> getToken: Fetched token: " + data[0].id + ", Access Level: " + data[0].accessLevel + ", Token holder: " + data[0].holder);
                    resolve(data[0]);
                }
            }).catch(err => {
                reject(err);
            });
        });
    },

    /* Getting token data when token holder is given */

    getTokenByHolder: (holder) => {
        return new Promise((resolve, reject) => {
            if(!holder) {

                /* token holder not provided */

                logger.error("module/token.js -> getTokenByHolder: token holder was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(holder.length !== 16) {

                /* invalid token holder length */

                logger.error("module/token.js -> getTokenByHolder: provided token holder had incorrect length");
                reject("INVALID_USER_ID");
                return;
            }
            r.table('tokens').filter({holder: holder}).coerceTo("array").run(connection).then(data => {
                if (data.length === 0) {

                    /* token not found */

                    logger.log("module/token.js -> getTokenByHolder: Token for " + holder + " was not found");
                    reject("TOKEN_NOT_FOUND");
                } else {

                    /* token is valid and exists, returning token data */

                    logger.log("module/token.js -> getTokenByHolder: Fetched token: " + data[0].id + ", Access Level: " + data[0].accessLevel + ", Token holder: " + data[0].holder);
                    resolve(data[0]);
                }
            }).catch(err => {
                reject(err);
            });
        });
    },

    /* Creating new token */

    newToken: (holder, accessLevel) => {
        return new Promise((resolve, reject) => {
            if(!holder) {

                /* token holder not provided */

                logger.error("module/token.js -> newToken: token holder was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(holder.length !== 16) {

                /* invalid token holder length */

                logger.error("module/token.js -> newToken: provided token holder had incorrect length");
                reject("INVALID_USER_ID");
                return;
            } else if(!accessLevel) {

                /* access level not provided */

                logger.error("module/token.js -> newToken: access level was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(accessLevel !== "USER" && accessLevel !== "COUNTER") {

                /* access level is not valid */

                logger.error("module/token.js -> newToken: access level is not valid");
                reject("INTERNAL_SERVER_ERROR");
                return;
            }
            r.table('tokens').insert({
                holder: holder,
                accessLevel: accessLevel
            }).run(connection).then(data => {
                if(data.errors !== 0) {

                    /* token creation failed */

                    logger.error("module/token.js -> newToken: token creation failed with an error: " + data.first_error);
                    reject(data.first_error);
                    return;
                }

                /* token created, returning new token */

                logger.log("module/token.js -> newToken: Created token: " + data.generated_keys[0] + ", Access Level: " + accessLevel + ", Token holder: " + holder);
                resolve(data.generated_keys[0]);
            }).catch(err => reject(err));
        });
    },

    deleteToken: (token) => {
        return new Promise((resolve, reject) => {
            r.table('tokens').filter({id: token}).delete().run(connection).then(result => {
                if(result.errors !== 0) {
                    logger.error("module/token.js -> deleteToken: " + result.first_error);
                    reject("INTERNAL_SERVER_ERROR");
                }
                logger.log("module/token.js -> deleteToken: token deleted");
                resolve();
            }).catch(err => logger.error(err));
        });
    }
};