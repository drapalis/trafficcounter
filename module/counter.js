const r = require('rethinkdb');
const counterModel = require('../models/counter');
const config = require('../config');
const logger = require('../module/log');
let tokenModule = require('../module/token');
let bcrypt = require('bcrypt');
let crypto = require('crypto');

let connection = null;
r.connect({host: config.host, port: config.port, db: config.database}, (err, conn) => {
    if (err) throw err;
    connection = conn;
});

module.exports = {

    /* getting counter data */

    getCounter: (id) => {
        return new Promise((resolve, reject) => {
            if(!id) {

                /* counterID not provided */

                logger.error("module/counter.js -> getCounter: counterID was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;
            } else if(id.length !== 16) {

                /* invalid userID length */

                logger.error("module/counter.js -> getCounter: provided counterID had incorrect length");
                reject("INVALID_COUNTER_ID");
                return;
            }
            r.table('counters').filter({counterID: id}).coerceTo("array").run(connection).then(data => {
                if(data.length === 0) {

                    /* counter was not found */

                    logger.log("module/counter.js -> getCounter: counter not found");
                    reject("COUNTER_NOT_FOUND");
                } else {

                    /* user was found, returning user data */

                    resolve(data[0]);
                }
            }).catch(err => reject(err));
        });
    },

    /* getting list of counters */

    getCountersOfUser: (userID) => {
        return new Promise((resolve, reject) => {
            r.table('counters').filter({userID: userID}).coerceTo("array").run(connection).then(data => {
                if(data.length === 0) {

                    /* no counters were found */

                    logger.log("module/counter.js -> getCounters: no counters found");
                    reject("NO_COUNTERS_FOUND");
                } else {

                    /* returning counter list with their data */

                    resolve(data);
                }
            }).catch(err => reject(err));
        });
    },

    /* getting list of counters */

    getCounters: () => {
        return new Promise((resolve, reject) => {
            r.table('counters').coerceTo("array").run(connection).then(data => {
                if(data.length === 0) {

                    /* no counters were found */

                    logger.log("module/counter.js -> getCounters: no counters found");
                    reject("NO_COUNTERS_FOUND");
                } else {

                    /* returning counter list with their data */

                    resolve(data);
                }
            }).catch(err => reject(err));
        });
    },

    /* getting list of counters of user */

    getCountersOfUser: (userID) => {
        return new Promise((resolve, reject) => {
            r.table('counters').filter({userID: userID}).coerceTo("array").run(connection).then(data => {
                if(data.length === 0) {

                    /* no counters were found */

                    logger.log("module/counter.js -> getCounters: no counters found");
                    reject("NO_COUNTERS_FOUND");
                } else {

                    /* returning counter list with their data */

                    resolve(data);
                }
            }).catch(err => reject(err));
        });
    },

    /* creating new counter */

    createCounter(holder, data) {
        return new Promise((resolve, reject) => {
            if(!holder || !data.url) {
                reject("INVALID_DATA");
                return;
            }

            /* removing id field */
            delete data.id;

            /* generating unique id for provided URL */
            data.counterID = bcrypt.hashSync(crypto.randomBytes(64).toString('hex'), config.emailSalt).substr(config.emailSalt.length,16).replace('\/','_');

            /* setting ownership */
            data.userID = holder;

            /* setting default settings if data is not provided */
            if(!data.title) data.title = '-';
            if(!data.description) data.description = '-';
            if(!data.backgroundColor) data.backgroundColor = '#FFFFFF';
            if(!data.foregroundColor) data.foregroundColor = '#000000';
            if(!data.views) data.views = 0;
            if(!data.uniqueViews) data.uniqueViews = 0;
            if(!data.viewsToday) data.viewsToday = 0;
            if(!data.uniqueViewsToday) data.uniqueViewsToday = 0;

            /* checking if counter with this url (id generated from url) already exists */

            r.table('counters').filter({counterID: data.counterID}).coerceTo('array').run(connection)
                .then(result => {
                    if(result.length === 0) {

                        /* counter does not exists, so now a new one can be created */

                        r.table('counters').insert(data).run(connection)
                            .then((result) => {
                                if(result.errors !== 0) {

                                    /* failed to create a new counter */

                                    logger.error("module/counter.js -> createCounter: Failed to create a new counter with an error: " + result.first_error);
                                    reject(result.first_error);
                                    return;
                                }

                                /* Counter successfully created */

                                logger.log("module/counter.js -> createCounter: Counter created: " + data.userID);

                                /* Creating an API token for new counter */

                                tokenModule.newToken(data.counterID, "COUNTER")
                                    .then(result => resolve({counter: {id: data.counterID, token: result}}))
                                    .catch(err => reject(err));
                            })
                            .catch(err => reject(err));
                    } else {

                        /* counter already exists */

                        logger.log("module/counter.js -> createCounter: Counter already exists: " + data.userID);
                        reject("COUNTER_ALREADY_EXISTS");
                    }
                })
                .catch(err => reject(err));

        });
    },

    /* updating counter data */

    updateCounter(counterID, data) {
        return new Promise((resolve, reject) => {
            if(!counterID) {
                reject("INVALID_DATA");
                return;
            }

            /* removing id field so it wouldn't be changed */
            delete data.id;

            /* deleting counter id so it wouldn't be changed */
            delete data.counterID;

            /* deleting ownership so it wouldn't be changed */
            delete data.userID;

            /* removing setting that were not provided */
            if(!data.url) delete data.url;
            if(!data.title) delete data.title;
            if(!data.description) delete data.description;
            if(!data.backgroundColor) delete data.backgroundColor;
            if(!data.foregroundColor) delete data.foregroundColor;

            /* users cannot change these settings */
            delete data.views;
            delete data.uniqueViews;
            delete data.viewsToday;
            delete data.uniqueViewsToday;

            /* checking if counter with this url (id generated from url) exists */

            r.table('counters').filter({counterID: counterID}).coerceTo('array').run(connection)
                .then(result => {
                    if(result.length !== 0) {

                        /* counter exists, so it can be updated */

                        r.table('counters').filter({counterID: counterID}).update(data).run(connection)
                            .then((result) => {
                                if(result.errors !== 0) {

                                    /* failed to create a new counter */

                                    logger.error("module/counter.js -> updateCounter: Failed to update a counter with an error: " + result.first_error);
                                    reject(result.first_error);
                                    return;
                                }

                                if(result.unchanged !== 0) {

                                    /* data was not changed */

                                    logger.error("module/counter.js -> updateCounter: No data was changed");
                                    reject("NOT_CHANGED");
                                } else {

                                    /* data updated */

                                    logger.log("module/counter.js -> updateCounter: Counter updated: " + counterID);
                                    resolve({UPDATED: data});

                                }
                            })
                            .catch(err => reject(err));
                    } else {

                        /* user already exists */

                        logger.log("module/counter.js -> updateCounter: Counter does not exists: " + counterID);
                        reject("COUNTER_NOT_FOUND");
                    }
                })
                .catch(err => reject(err));

        });
    },

    /* delete counter */

    deleteCounter: (id) => {
        return new Promise((resolve, reject) => {
            if(!id) {

                /* counterID not provided */

                logger.error("module/counter.js -> deleteCounter: counterID was not provided");
                reject("INTERNAL_SERVER_ERROR");
                return;

            } else if(id.length !== 16) {

                /* invalid counterID length */

                logger.error("module/counter.js -> deleteCounter: provided counterID had incorrect length");
                reject("INVALID_COUNTER_ID");
                return;
            }
            /* getting counter data */
            r.table('counters').filter({counterID: id}).coerceTo("array").run(connection).then(counterData => {
                if(counterData.length === 0) {

                    /* counter was not found */

                    logger.log("module/counter.js -> deleteCounter: counter not found");
                    reject("COUNTER_NOT_FOUND");
                }

                counterData = counterData[0];

                /* deleting counter stats */
                r.table('stats').filter({counterID: counterData.counterID}).delete().run(connection).then(result => {
                    if(result.errors !== 0) {
                        logger.error("module/counter.js -> deleteCounter: " + result.first_error);
                    }
                    logger.log("module/counter.js -> deleteCounter: counter data from 'stats' deleted");
                }).catch(err => logger.error(err));
                /* deleting counter token */
                r.table('tokens').filter({holder: counterData.counterID}).delete().run(connection).then(result => {
                    if(result.errors !== 0) {
                        logger.error("module/counter.js -> deleteCounter: " + result.first_error);
                    }
                    logger.log("module/counter.js -> deleteCounter: counter data from 'tokens' deleted");
                }).catch(err => logger.error(err));
                /* deleting counter */
                r.table('counters').filter({counterID: counterData.counterID}).delete().run(connection).then(result => {
                    if(result.errors !== 0) {
                        logger.error("module/counter.js -> deleteCounter: " + result.first_error);
                    }
                    logger.log("module/counter.js -> deleteCounter: counter data from 'counters' deleted");
                }).catch(err => logger.error(err));
                resolve("COUNTER_DELETED");

            }).catch(err => reject(err));
        });
    },

    addView: (id) => {
        module.exports.getCounter(id).then(result => {
            r.table('counters').filter({counterID: id}).update({
                views: result.views + 1,
                viewsToday: result.viewsToday + 1
            }).run(connection).then(result => {
                logger.log("Added one view to counter: " + id);
            }).catch(err => logger.error(err));
        }).catch(err => logger.error(err));
    },

    addUniqueView: (id) => {
        module.exports.getCounter(id).then(result => {
            r.table('counters').filter({counterID: id}).update({
                uniqueViews: result.uniqueViews + 1,
                uniqueViewsToday: result.uniqueViewsToday + 1
            }).run(connection).then(result => {
                logger.log("Added one view to counter: " + id);
            }).catch(err => logger.error(err));
        }).catch(err => logger.error(err));
    }
};