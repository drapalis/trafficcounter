module.exports = {
    response: (res, status, data) => {
        res.status(status);
        res.json({RESPONSE: data});
        console.log(status, {RESPONSE: data});
    },
    OK: (res, data) => {
        module.exports.response(res, 200, data);
    },
    CREATED: (res, data) => {
        module.exports.response(res, 201, data);
    },
    ACCEPTED: (res, data) => {
        module.exports.response(res, 202, data);
    },
    NO_CONTENT: (res, data) => {
        module.exports.response(res, 204, data);
    },
    FOUND: (res, data) => {
        module.exports.response(res, 302, data);
    },
    NOT_MODIFIED: (res, data) => {
        module.exports.response(res, 304, data);
    },
    BAD_REQUEST: (res, data) => {
        module.exports.response(res, 400, data);
    },
    UNAUTHORIZED: (res, data) => {
        module.exports.response(res, 401, data);
    },
    FORBIDDEN: (res, data) => {
        module.exports.response(res, 403, data);
    },
    NOT_FOUND: (res, data) => {
        module.exports.response(res, 404, data);
    },
    NOT_ACCEPTABLE: (res, data) => {
        module.exports.response(res, 406, data);
    },
    GONE: (res, data) => {
        module.exports.response(res, 410, data);
    },
    TOO_MANY_REQUESTS: (res, data) => {
        module.exports.response(res, 429, data);
    },
    INTERNAL_SERVER_ERROR: (res, data) => {
        module.exports.response(res, 500, data);
    },
    BAD_GATEWAY: (res, data) => {
        module.exports.response(res, 502, data);
    },
    SERVICE_UNAVAILABLE: (res, data) => {
        module.exports.response(res, 503, data);
    },
    GATEWAY_TIMEOUT: (res, data) => {
        module.exports.response(res, 504, data);
    },
    accessDenied(res) {
        module.exports.FORBIDDEN(res, {ERROR: "Access denied!"});
    },
    invalidRequest(res) {
        module.exports.BAD_REQUEST(res, {ERROR: "Invalid request"});
    },
    tooManyRequests(res) {
        module.exports.TOO_MANY_REQUESTS(res, {ERROR: "Too many requests"});
    },
    manageResponse(res, data) {
        switch (data) {
            case "INTERNAL_SERVER_ERROR":
                module.exports.INTERNAL_SERVER_ERROR(res, {ERROR: "Internal server error"});
                break;
            case "USER_NOT_FOUND":
                module.exports.NOT_FOUND(res, {ERROR: "User not found"});
                break;
            case "COUNTER_NOT_FOUND":
                module.exports.NOT_FOUND(res, {ERROR: "Counter not found"});
                break;
            case "TOKEN_NOT_FOUND":
                module.exports.NOT_FOUND(res, {ERROR: "Token not found"});
                break;
            case "INVALID_API_TOKEN":
                module.exports.UNAUTHORIZED(res, {ERROR: "Invalid API token"});
                break;
            case "INVALID_USER_ID":
                module.exports.UNAUTHORIZED(res, {ERROR: "Invalid user id"});
                break;
            case "INVALID_COUNTER_ID":
                module.exports.UNAUTHORIZED(res, {ERROR: "Invalid counter id"});
                break;
            case "ACCESS_DENIED":
                module.exports.FORBIDDEN(res, {ERROR: "Access denied!"});
                break;
            case "USER_ALREADY_EXISTS":
                module.exports.ACCEPTED(res, {ERROR: "User already exists"});
                break;
            case "NOT_CHANGED":
                module.exports.OK(res, {RESULT: "No data was changed"});
                break;
            case "NO_DATA_PROVIDED":
                module.exports.BAD_REQUEST(res, {ERROR: "Invalid request, no data provided!"});
                break;
            case "INVALID_DATA":
                module.exports.BAD_REQUEST(res, {ERROR: "Invalid request data!"});
                break;
            case "NO_TOKEN_PROVIDED":
                module.exports.BAD_REQUEST(res, {ERROR: "No token provided!"});
                break;
            case "NO_USERS_FOUND":
                module.exports.NOT_FOUND(res, {ERROR: "No users found"});
                break;
            case "NO_COUNTERS_FOUND":
                module.exports.NOT_FOUND(res, {ERROR: "No counters found"});
                break;
            case "COUNTER_ALREADY_EXISTS":
                module.exports.ACCEPTED(res, {ERROR: "Counter already exists"});
                break;
            default:
                module.exports.INTERNAL_SERVER_ERROR(res, {ERROR: data});
        }
    }
};